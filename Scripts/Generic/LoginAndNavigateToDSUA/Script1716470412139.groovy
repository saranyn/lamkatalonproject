import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.WebDriver as WebDriver
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.aventstack.extentreports.Status as Status
import com.kms.katalon.core.exception.StepErrorException as StepErrorException
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Generic/LoginTestCase'), [('username') : GlobalVariable.G_Username, ('password') : GlobalVariable.G_Password
        , ('TestDescription') : 'Login to LAM', ('testType') : '', ('module') : GlobalVariable.G_Module, ('elementToVerify') : '//div[@class=\'header-logo sluas-logo\']'], 
    FailureHandling.STOP_ON_FAILURE)

def extentTest = GlobalVariable.G_ExtentTest

try {
    WebDriver driver = DriverFactory.getWebDriver()

    WebUI.click(findTestObject('Object Repository/Page_Altair_SLUAS/apps_rounded_menu_icon'))

    extentTest.log(Status.PASS, 'Click application menu icon')

    module_test_obj = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForModule'(GlobalVariable.G_Module)

    CustomKeywords.'loginPackage.LoginKeyword.navigateToModule'(driver, extentTest, module_test_obj, dsua_url)

    TestObject testObj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObj'(elementToVerify)

    WebUI.verifyElementVisible(testObj)

    extentTest.log(Status.PASS, 'User logged in Sucessfully and landed in Altair DSUA application')

    driver.close()
}
catch (Exception ex) {
    String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'

    WebUI.takeScreenshot(screenShotPath)

    String p = (TestDescription + GlobalVariable.G_Browser) + '.png'

    extentTest.log(Status.FAIL, 'User login into DSUA Module Failed')

    extentTest.log(Status.FAIL, ex)

    extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())

    KeywordUtil.markFailed('Exception Error: ' + ex)
} 
catch (StepErrorException e) {
    String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'

    WebUI.takeScreenshot(screenShotPath)

    String p = (TestDescription + GlobalVariable.G_Browser) + '.png'

    extentTest.log(Status.FAIL, 'User login into DSUA Module Failed')

    extentTest.log(Status.FAIL, ex)

    extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())

    KeywordUtil.markFailed('StepErrorException Error: ' + e)
} 

