import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import groovy.console.ui.SystemOutputInterceptor
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.WebDriver as WebDriver
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.aventstack.extentreports.Status as Status
import com.kms.katalon.core.exception.StepErrorException as StepErrorException
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement

WebUI.callTestCase(findTestCase('Generic/LoginTestCase'), [('username') : GlobalVariable.G_Username, ('password') : GlobalVariable.G_Password
, ('TestDescription') : 'Login to LAM', ('testType') : '', ('module') : GlobalVariable.G_Module, ('elementToVerify') : '//div[@class=\'header-logo sluas-logo\']'],
FailureHandling.STOP_ON_FAILURE)

def extentTest = GlobalVariable.G_ExtentTest

try {
	WebDriver driver = DriverFactory.getWebDriver()
	WebUI.click(findTestObject('Object Repository/Page_Altair_SLUAS/apps_rounded_menu_icon'))
	module_test_obj = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForModule'(GlobalVariable.G_Module)
	CustomKeywords.'loginPackage.LoginKeyword.navigateToModule'(driver, extentTest, module_test_obj, dsua_url)
	TestObject testObj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObj'(elementToVerify)
	WebUI.verifyElementVisible(testObj)
	WebUI.click(findTestObject('Object Repository/Page_Altair_DSUA/computers'))
	WebUI.click(findTestObject('Object Repository/Page_DSUA_Computers/add_new_computer_button'))
	WebUI.scrollToElement(findTestObject('Object Repository/Page_DSUA_Computers/dsua_agent_2024_1_b1_button'), 5)
	WebUI.click(findTestObject('Object Repository/Page_DSUA_Computers/dsua_agent_2024_1_b1_button'))
	//Read document
	File dir = new File(System.getProperty('user.home') + '//Downloads')
	
	//set delay of 20 seconds to give ample time to download (8mb of file)
	System.out.println("The DSUA Agent is still downloading. Please wait to finish download")
	WebUI.delay(20)
	File[] files = dir.listFiles(new FileFilter() {
	@Override
	public boolean accept(File pathname) {
		return pathname.getName().startsWith(downloaded_filename);
		}
	});
	
	// Get recent file
	String latestFileName = (files[(files.length - 1)]).getName()
	System.out.println(latestFileName)
	//Assert file is downloaded to local machine
	assert latestFileName.contains(downloaded_filename)
}

catch (Exception ex) {
	String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'
}

catch (StepErrorException e) {
	String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'

	WebUI.takeScreenshot(screenShotPath)

	String p = (TestDescription + GlobalVariable.G_Browser) + '.png'

	extentTest.log(Status.FAIL, 'User login into DSUA Module Failed')
	
	extentTest.log(Status.FAIL, ex)
	
	extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())
	
	KeywordUtil.markFailed('StepErrorException Error: ' + e)
}
