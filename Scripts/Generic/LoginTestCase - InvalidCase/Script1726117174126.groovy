import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys

import com.aventstack.extentreports.MediaEntityBuilder
import com.aventstack.extentreports.Status
import com.kms.katalon.core.exception.StepErrorException
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

def extentTest = GlobalVariable.G_ExtentTest

try {

	extentTest.log(Status.PASS, 'Navigated to url - ' +GlobalVariable.G_BaseUrl)
		
	GlobalVariable.G_Username=username
	GlobalVariable.G_Password=password
	CustomKeywords.'loginPackage.LoginKeyword.login'(extentTest, GlobalVariable.G_Username, GlobalVariable.G_Password)
	
	String actual_error = CustomKeywords.'loginPackage.LoginKeyword.loginInvalid'(extentTest, GlobalVariable.G_Username, GlobalVariable.G_Password)
	
	assert actual_error == elementToVerify
	
    extentTest.log(Status.PASS, 'Invalid Login Verified')
		
}
catch (Exception ex) {
	String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'
	WebUI.takeScreenshot(screenShotPath)
	String p = TestDescription+GlobalVariable.G_Browser+'.png'
	
	extentTest.log(Status.FAIL, 'User login Failed')
	extentTest.log(Status.FAIL,ex)
	extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())

	KeywordUtil.markFailed('Exception Error: ' + ex)
}
catch (StepErrorException e) {
	String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'
	WebUI.takeScreenshot(screenShotPath)
	
	extentTest.log(Status.FAIL, 'User login Failed')
	extentTest.log(Status.FAIL,ex)
	extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())

	KeywordUtil.markFailed('StepErrorException Error: ' + e)
}