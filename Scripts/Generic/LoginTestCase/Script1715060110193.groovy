import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys

import com.aventstack.extentreports.MediaEntityBuilder
import com.aventstack.extentreports.Status
import com.kms.katalon.core.exception.StepErrorException
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

def extentTest = GlobalVariable.G_ExtentTest

try {

	extentTest.log(Status.PASS, 'Navigated to url - ' +GlobalVariable.G_BaseUrl)
		
	GlobalVariable.G_Username=username
	GlobalVariable.G_Password=password
	CustomKeywords.'loginPackage.LoginKeyword.login'(extentTest, GlobalVariable.G_Username, GlobalVariable.G_Password)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Altair_SLUAS/windows_agent'), 5)

	WebUI.sendKeys(findTestObject('Object Repository/Page_Altair_SLUAS/windows_agent'), Keys.chord(Keys.ESCAPE))

	TestObject testObj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObj'(elementToVerify)
		
	WebUI.verifyElementVisible(testObj)
    extentTest.log(Status.PASS, 'User logged in Sucessfully and landed in Altair SLUAS application')
		
}
catch (Exception ex) {
	String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'
	WebUI.takeScreenshot(screenShotPath)
	String p = TestDescription+GlobalVariable.G_Browser+'.png'
	
	extentTest.log(Status.FAIL, 'User login Failed')
	extentTest.log(Status.FAIL,ex)
	extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())

	KeywordUtil.markFailed('Exception Error: ' + ex)
}
catch (StepErrorException e) {
	String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'
	WebUI.takeScreenshot(screenShotPath)
	
	extentTest.log(Status.FAIL, 'User login Failed')
	extentTest.log(Status.FAIL,ex)
	extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())

	KeywordUtil.markFailed('StepErrorException Error: ' + e)
}