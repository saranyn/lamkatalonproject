import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import groovy.console.ui.SystemOutputInterceptor
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.WebDriver as WebDriver
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.aventstack.extentreports.Status as Status
import com.kms.katalon.core.exception.StepErrorException as StepErrorException
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement

WebUI.callTestCase(findTestCase('Generic/LoginTestCase'), [('username') : GlobalVariable.G_Username, ('password') : GlobalVariable.G_Password
, ('TestDescription') : 'Login to LAM', ('testType') : '', ('module') : GlobalVariable.G_Module, ('elementToVerify') : '//div[@class=\'header-logo sluas-logo\']'],
FailureHandling.STOP_ON_FAILURE)

def extentTest = GlobalVariable.G_ExtentTest

try {
	WebDriver driver = DriverFactory.getWebDriver()
	WebUI.click(findTestObject('Object Repository/Page_Altair_SLUAS/apps_rounded_menu_icon'))
	module_test_obj = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForModule'(GlobalVariable.G_Module)
	CustomKeywords.'loginPackage.LoginKeyword.navigateToModule'(driver, extentTest, module_test_obj, dsua_url)
	TestObject testObj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObj'(elementToVerify)
	WebUI.verifyElementVisible(testObj)
	WebUI.delay(3)
	WebUI.click(findTestObject('Object Repository/Page_Altair_DSUA/usage_history'))
	WebUI.setText(findTestObject('Object Repository/Page_DSUA_UsageHistory/package_name_textfield'), package_name)
	WebUI.delay(1)
	WebUI.click(findTestObject('Object Repository/Page_DSUA_UsageHistory/filters_apply_button'))
	WebUI.delay(3)
	WebElement table = driver.findElement(By.xpath("//table/tbody"))
	List<WebElement> rows_table = table.findElements(By.tagName('tr'))
	int rows_count = rows_table.size()
	//To locate columns(cells) of that specific row
	List<WebElement> columns_row = rows_table.get(0).findElements(By.tagName('td'))
	//	//To calculate no of columns(cells) In that specific row'
	int columns_count = columns_row.size()
	if(columns_count != 0) {
	for(int i=0; i<columns_count; i++) {
	String packageName = columns_row.get(i).getText();
	if(packageName == package_name) {
	assert true;
	extentTest.log(Status.PASS, 'Package Name: '+packageName+' is found!')
	}
	else {
	extentTest.log(Status.FAIL, 'Package Name: '+packageName+' not found!')
	}
	}
	}
	else {
	extentTest.log(Status.FAIL, 'No entries found with the package name '+package_name)
	}
	//	System.out.println(packageName)
	//	assert package_name == packageName
}
catch (Exception ex) {
	String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'
}
catch (StepErrorException e) {
	String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'
	
	WebUI.takeScreenshot(screenShotPath)
	
	String p = (TestDescription + GlobalVariable.G_Browser) + '.png'
	
	extentTest.log(Status.FAIL, 'User login into DSUA Module Failed')
	
	extentTest.log(Status.FAIL, ex)
	
	extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())
	
	KeywordUtil.markFailed('StepErrorException Error: ' + e)
}
