import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.assertthat.selenium_shutterbug.utils.web.Browser as Browser
import com.kms.katalon.core.exception.StepErrorException as StepErrorException
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword as WebUIAbstractKeyword
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder

WebUI.callTestCase(findTestCase('Generic/LoginTestCase'), [('username') : GlobalVariable.G_Username, ('password') : GlobalVariable.G_Password
        , ('TestDescription') : 'Login to LAM', ('testType') : '', ('module') : GlobalVariable.G_Module, ('elementToVerify') : '//div[@class=\'header-logo sluas-logo\']'], 
    FailureHandling.STOP_ON_FAILURE)

def extentTest = GlobalVariable.G_ExtentTest

try {
    WebUI.click(findTestObject('Object Repository/Page_Altair_SLUAS/profile_avatar'))

    extentTest.log(Status.PASS, 'Click Profile Avatar')

    WebUI.click(findTestObject('Object Repository/Page_Altair_SLUAS/logout_button'))

    extentTest.log(Status.PASS, 'Click Profile Logout Button')

    TestObject testObj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObj'(elementToVerify)

    //    def isElementPresent = CustomKeywords.'customWait.WaitForElement.WaitForelementPresent'(testObj, 10, extentTest, 'Altair SLUAS Logo In Logout Page')
    WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Logout/login_again_link'), 10)

    extentTest.log(Status.PASS, 'Login again link is verified')

    extentTest.log(Status.PASS, 'User logged out Sucessfully')
}
catch (Exception ex) {
    String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'

    WebUI.takeScreenshot(screenShotPath)

    String p = (TestDescription + GlobalVariable.G_Browser) + '.png'

    extentTest.log(Status.FAIL, 'User log out Failed')

    extentTest.log(Status.FAIL, ex)

    extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())

    KeywordUtil.markFailed('Exception Error: ' + ex)
} 
catch (StepErrorException e) {
    String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'

    WebUI.takeScreenshot(screenShotPath)

    String p = (TestDescription + GlobalVariable.G_Browser) + '.png'

    extentTest.log(Status.FAIL, 'User log out Failed')

    extentTest.log(Status.FAIL, ex)

    extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())

    KeywordUtil.markFailed('StepErrorException Error: ' + e)
} 

