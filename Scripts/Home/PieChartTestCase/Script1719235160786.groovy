import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.assertthat.selenium_shutterbug.utils.web.Browser as Browser
import com.kms.katalon.core.exception.StepErrorException as StepErrorException
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword as WebUIAbstractKeyword
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import groovy.json.StringEscapeUtils

import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import com.assertthat.selenium_shutterbug.core.Shutterbug
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import java.io.File
import org.openqa.selenium.WebElement
import org.openqa.selenium.By
import org.openqa.selenium.interactions.Actions;
import com.kms.katalon.core.configuration.RunConfiguration
import org.openqa.selenium.JavascriptExecutor;

WebUI.callTestCase(findTestCase('Generic/LoginTestCase'), [('username') : GlobalVariable.G_Username, ('password') : GlobalVariable.G_Password
        , ('TestDescription') : 'Login to LAM', ('testType') : '', ('module') : GlobalVariable.G_Module, ('elementToVerify') : '//div[@class=\'header-logo sluas-logo\']'], 
    FailureHandling.STOP_ON_FAILURE)

def extentTest = GlobalVariable.G_ExtentTest

try {
    WebDriver driver = DriverFactory.getWebDriver()

    WebUI.click(findTestObject('Object Repository/Page_Altair_SLUAS/apps_rounded_menu_icon'))

    extentTest.log(Status.PASS, 'Click application menu icon')

    module_test_obj = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForModule'(GlobalVariable.G_Module)

    CustomKeywords.'loginPackage.LoginKeyword.navigateToModule'(driver, extentTest, module_test_obj, dsua_url)

    TestObject testObj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObj'(elementToVerify)

    WebUI.verifyElementVisible(testObj)

    extentTest.log(Status.PASS, 'User logged in Sucessfully and landed in Altair DSUA application')
	
	WebUI.click(findTestObject('Object Repository/Page_Altair_DSUA/home'))
	
	WebElement canvasElement = driver.findElement(By.id(elementId))	
	
	
//	Shutterbug.shootElement(driver, canvasElement).save(RunConfiguration.getProjectDir() + "/ExpectedScreenshots/")	
	
	// Load the reference image
//	String expectedImagePath = RunConfiguration.getProjectDir() + "/ExpectedScreenshots/" + fileName + "_Expected_Image.png";
//	
//	BufferedImage expectedImage = ImageIO.read(new File(expectedImagePath))
//	
//	String diffImage = RunConfiguration.getProjectDir() + "/DiffScreenshots/" + fileName + "_Diff_Image.png";
	
	// Compare images
//	boolean imagesAreSame = Shutterbug.shootElement(driver, canvasElement).equalsWithDiff(expectedImage, diffImage)
//	println imagesAreSame
//	
//	if (imagesAreSame) {
//		KeywordUtil.markPassed('Images are the same')
//	} else {
//		KeywordUtil.markFailed('Images are different')
//	}
//	
	WebUI.delay(3)
	
	WebElement hoverElement = driver.findElement(By.xpath("//div[@id='"+elementId+"']//canvas"));
	println "hoverElement: "+hoverElement
	Actions action = new Actions(driver)

	action.moveToElement(hoverElement).perform();

	
	WebUI.delay(10)
		
	switch(fileName) {
		
		case 'ComputersByStatus':

			println "inside computer by status"

			TestObject onlineEle = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForComputersByStatus'(elementId, hoverElement1)
			TestObject offlineEle = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForComputersByStatus'(elementId, hoverElement2)

			if (WebUI.verifyElementPresent(onlineEle, 5, FailureHandling.OPTIONAL) == true) {
				String onlineCountFromChartStr = driver.findElement(By.xpath("//div[@id='"+elementId+"']//div//span[text()='"+hoverElement1+"']/following-sibling::span")).getText()
				int onlineCountFromChart = Integer.parseInt(onlineCountFromChartStr);
				println onlineCountFromChart

				canvasElement.click()

				List<WebElement> computerTableListOnlineStatus = driver.findElements(By.xpath("//tbody//tr//td[1]//*[local-name()='svg' and @data-testid='CheckCircleRoundedIcon']"))
				int computerTableListCount = computerTableListOnlineStatus.size()
				println (computerTableListCount)

				assert computerTableListCount == onlineCountFromChart

			}else if (WebUI.verifyElementPresent(offlineEle, 5, FailureHandling.OPTIONAL) == true){
				String offlineCountFromChartStr = driver.findElement(By.xpath("//div[@id='"+elementId+"']//div//span[text()='"+hoverElement2+"']/following-sibling::span")).getText()
				int offlineCountFromChart = Integer.parseInt(offlineCountFromChartStr);
				println offlineCountFromChart

				canvasElement.click()

				List<WebElement> computerTableListOfflineStatus = driver.findElements(By.xpath("//tbody//tr//td[1]//*[local-name()='svg' and @data-testid='HighlightOffRoundedIcon']"))
				int computerTableListCount = computerTableListOfflineStatus.size()
				println (computerTableListCount)

				assert computerTableListCount == offlineCountFromChart
			}

		case 'ComputersByTimezone':

			println "inside computer by time zone"

			TestObject utc1Ele = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForComputersByStatus'(elementId, hoverElement1)
			TestObject utc2Ele = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForComputersByStatus'(elementId, hoverElement2)

			if (WebUI.verifyElementPresent(utc1Ele, 5, FailureHandling.OPTIONAL) == true) {
				String utc1FromChartStr = driver.findElement(By.xpath("//div[@id='"+elementId+"']//div//span[text()='"+hoverElement1+"']/following-sibling::span")).getText()
				int utc1FromChart = Integer.parseInt(utc1FromChartStr);
				println utc1FromChart

				canvasElement.click()

				List<WebElement> utc1 = driver.findElements(By.xpath("//span[@title='"+hoverElement1+"'"))
				int utc1ListCount = utc1.size()
				println (utc1ListCount)

				assert utc1ListCount == utc1FromChart

			}else if (WebUI.verifyElementPresent(utc2Ele, 5, FailureHandling.OPTIONAL) == true){
				String utc2FromChartStr = driver.findElement(By.xpath("//div[@id='"+elementId+"']//div//span[text()='"+hoverElement2+"']/following-sibling::span")).getText()
				int utc2CountFromChart = Integer.parseInt(utc2FromChartStr);
				println utc2CountFromChart

				canvasElement.click()

				List<WebElement> utc2 = driver.findElements(By.xpath("//span[@title='"+hoverElement2+"'"))
				int utc2ListCount = utc2.size()
				println (utc2ListCount)

				assert utc2ListCount == utc2CountFromChart
			}

		case 'TrackedPackageStatus':

			println "inside tracked package status"

			TestObject runningEle = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForComputersByStatus'(elementId, hoverElement1)
			TestObject notRunningEle = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForComputersByStatus'(elementId, hoverElement2)

			if (WebUI.verifyElementPresent(runningEle, 5, FailureHandling.OPTIONAL) == true) {
				String runningCountFromChartStr = driver.findElement(By.xpath("//div[@id='"+elementId+"']//div//span[text()='"+hoverElement1+"']/following-sibling::span")).getText()
				int runningCountFromChart = Integer.parseInt(runningCountFromChartStr);
				println runningCountFromChart

				canvasElement.click()

				List<WebElement> activeUsageList = driver.findElements(By.xpath("//div[@id='active-usage-table']//table//tr//td[1]"))
				int activeUsageListCount = activeUsageList.size()
				println (activeUsageListCount)

				assert runningCountFromChart == activeUsageListCount

			}else if (WebUI.verifyElementPresent(notRunningEle, 5, FailureHandling.OPTIONAL) == true){
				String notRunningCountFromChartStr = driver.findElement(By.xpath("//div[@id='"+elementId+"']//div//span[text()='"+hoverElement2+"']/following-sibling::span")).getText()
				int notRunningCountFromChart = Integer.parseInt(notRunningCountFromChartStr);
				println notRunningCountFromChart

				canvasElement.click()

				List<WebElement> packagesList = driver.findElements(By.xpath("//tbody/tr/td[3]/span/following::*[name()='svg' and @data-testid='EditRoundedIcon'][1]"))
				int packagesListcount = packagesList.size()
				println (packagesListcount)

				//Active Usage Page
				WebUI.click(findTestObject('Object Repository/Page_Altair_DSUA/active_usage'))

				List<WebElement> activeUsageList = driver.findElements(By.xpath("//div[@id='active-usage-table']//table//tr//td[1]"))
				int activeUsageListCount = activeUsageList.size()
				println (activeUsageListCount)
				int runningCountFromChart = activeUsageListCount

				assert packagesListcount-runningCountFromChart == notRunningCountFromChart
			}

		case 'TrackedPackageRunningInstancesStatus':

			println "inside tracked package running instances status"

			TestObject idleEle = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForComputersByStatus'(elementId, hoverElement1)
			TestObject activeEle = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForComputersByStatus'(elementId, hoverElement2)

			if (WebUI.verifyElementPresent(idleEle, 5, FailureHandling.OPTIONAL) == true) {
				
				String idleCountFromChartStr = driver.findElement(By.xpath("//div[@id='"+elementId+"']//div//span[text()='"+hoverElement1+"']/following-sibling::span")).getText()
				int idleCountFromChart = Integer.parseInt(idleCountFromChartStr);
				println idleCountFromChart

				canvasElement.click()

				String idleList = driver.findElement(By.xpath("//div[@id='active-usage-table']//table//tr//td[4]")).getText()
				int idleListCount = Integer.parseInt(idleList)
				println (idleListCount)

				assert idleCountFromChart == idleListCount

			}else if (WebUI.verifyElementPresent(activeEle, 5, FailureHandling.OPTIONAL) == true){
				
				//this code will not work - Check this how to get active count and then verify
				String activeCountFromChartStr = driver.findElement(By.xpath("//div[@id='"+elementId+"']//div//span[text()='"+hoverElement2+"']/following-sibling::span")).getText()
				int activeCountFromChart = Integer.parseInt(activeCountFromChartStr);
				println activeCountFromChart

				canvasElement.click()

				List<WebElement> activeList = driver.findElements(By.xpath("//tbody/tr/td[3]/span/following::*[name()='svg' and @data-testid='EditRoundedIcon'][1]"))
				int activeListcount = activeList.size()
				println (activeListcount)

				assert activeListcount == activeCountFromChart
			}
	}

    driver.close()
}
catch (Exception ex) {
    String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'

    WebUI.takeScreenshot(screenShotPath)

    String p = (TestDescription + GlobalVariable.G_Browser) + '.png'

    extentTest.log(Status.FAIL, TestDescription + ' is failing')

    extentTest.log(Status.FAIL, ex)

    extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())

    KeywordUtil.markFailed('Exception Error: ' + ex)
} 
catch (StepErrorException e) {
    String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'

    WebUI.takeScreenshot(screenShotPath)

    String p = (TestDescription + GlobalVariable.G_Browser) + '.png'

    extentTest.log(Status.FAIL, TestDescription + ' is failing')

    extentTest.log(Status.FAIL, ex)

    extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())

    KeywordUtil.markFailed('StepErrorException Error: ' + e)
} 

