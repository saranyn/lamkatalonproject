import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.assertthat.selenium_shutterbug.utils.web.Browser as Browser
import com.kms.katalon.core.exception.StepErrorException as StepErrorException
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword as WebUIAbstractKeyword
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver

WebUI.callTestCase(findTestCase('Generic/LoginTestCase'), [('username') : GlobalVariable.G_Username, ('password') : GlobalVariable.G_Password
        , ('TestDescription') : 'Login to LAM', ('testType') : '', ('module') : GlobalVariable.G_Module, ('elementToVerify') : '//div[@class=\'header-logo sluas-logo\']'], 
    FailureHandling.STOP_ON_FAILURE)

def extentTest = GlobalVariable.G_ExtentTest

try {
    WebDriver driver = DriverFactory.getWebDriver()

    WebUI.click(findTestObject('Object Repository/Page_Altair_SLUAS/apps_rounded_menu_icon'))

    extentTest.log(Status.PASS, 'Click application menu icon')

    module_test_obj = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForModule'(GlobalVariable.G_Module)

    CustomKeywords.'loginPackage.LoginKeyword.navigateToModule'(driver, extentTest, module_test_obj, dsua_url)

    TestObject testObj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObj'(elementToVerify)

    WebUI.verifyElementVisible(testObj)

    extentTest.log(Status.PASS, 'User logged in Sucessfully and landed in Altair DSUA application')

    //WebUI.click(findTestObject('Object Repository/Page_Altair_DSUA/settings_icon'))
    //extentTest.log(Status.PASS, 'click settings icon')
    WebUI.click(findTestObject('Object Repository/Page_Altair_DSUA/user_icon_menu'))

    extentTest.log(Status.PASS, 'click user menu icon')

    WebUI.click(findTestObject('Object Repository/Page_Altair_DSUA/package_setup'))

    extentTest.log(Status.PASS, 'click Package Setup')

    //switch statement
    switch (userChoice) {
        case 'toggleStatus':
            //BUG: The pop up in the UI doesn't go away, until user clicks outside
            CustomKeywords.'addPackageKeyword.AddPackageKeyword.addPackage'(extentTest, publisherName, packageName, installationLocation, 
                licenseFeaturePatterns, 'save')

            extentTest.log(Status.PASS, 'Package saved successfully with publisher: ' + publisherName)

            TestObject publisher_obj = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForPublisher'(publisherName)

            WebUI.scrollToElement(publisher_obj, 10)

            extentTest.log(Status.PASS, 'Scroll to publisher: ' + publisherName)

            WebUI.verifyElementPresent(publisher_obj, 10)

            extentTest.log(Status.PASS, 'Verified created publisher in the table: ' + publisherName)

            TestObject package_obj = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForPackage'(packageName)

            WebUI.verifyElementPresent(package_obj, 10)

            extentTest.log(Status.PASS, 'Verified created package in the table: ' + packageName)

            TestObject tracking_toggle_obj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObjForToggleButtonsForPackage'(
                packageName, 'tracking')

            boolean isTrackingToggleChecked = WebUI.verifyElementChecked(tracking_toggle_obj, 10)

            assert isTrackingToggleChecked == true

            extentTest.log(Status.PASS, 'Verified tracking toggle is checked by default: ' + isTrackingToggleChecked)

            TestObject inventory_toggle_obj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObjForToggleButtonsForPackage'(
                packageName, 'inventory')

            boolean isInventoryToggleChecked = WebUI.verifyElementChecked(inventory_toggle_obj, 10)

            assert isInventoryToggleChecked == true

            extentTest.log(Status.PASS, 'Verified inventory toggle is checked by default: ' + isInventoryToggleChecked)

            break
        case 'cancel':
            CustomKeywords.'addPackageKeyword.AddPackageKeyword.addPackage'(extentTest, publisherName, packageName, installationLocation, 
                licenseFeaturePatterns, 'cancel')

            TestObject publisher_obj = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForPublisher'(elementToVerify1)

            WebUI.verifyElementNotPresent(publisher_obj, 10)

            extentTest.log(Status.PASS, 'Verified publisher is not created by canceling the package creation')

            break
        case 'inlineValidationForPublisherName':
            CustomKeywords.'addPackageKeyword.AddPackageKeyword.addPackage'(extentTest, publisherName, packageName, installationLocation, 
                licenseFeaturePatterns, 'save')

            TestObject publisher_error_obj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObj'(elementToVerify1)

            //TODO: make sure once id is added get error text and verify and display it in report log
            WebUI.verifyElementVisible(publisher_error_obj)

            extentTest.log(Status.PASS, 'Verified Inline Validation For PublisherName')

            break
        case 'inlineValidationForPackageName':
            CustomKeywords.'addPackageKeyword.AddPackageKeyword.addPackage'(extentTest, publisherName, packageName, installationLocation, 
                licenseFeaturePatterns, 'save')

            TestObject package_error_obj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObj'(elementToVerify1)

            //TODO: make sure once id is added get error text and verify and display it in report log
            WebUI.verifyElementPresent(package_error_obj, 10)

            extentTest.log(Status.PASS, 'Verified Inline Validation For PackageName')

            break
        case 'inlineValidationForInstallationLocation':
            CustomKeywords.'addPackageKeyword.AddPackageKeyword.addPackage'(extentTest, publisherName, packageName, installationLocation, 
                licenseFeaturePatterns, 'save')

            TestObject install_error_obj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObj'(elementToVerify1)

            //TODO: make sure once id is added get error text and verify and display it in report log
            WebUI.verifyElementPresent(install_error_obj, 10)

            extentTest.log(Status.PASS, 'Verified Inline Validation For InstallationLocation')

            break
        case 'inlineValidationForEmptyPublisherName':
            CustomKeywords.'addPackageKeyword.AddPackageKeyword.addPackage'(extentTest, publisherName, packageName, installationLocation, 
                licenseFeaturePatterns, 'save')

            TestObject publisher_obj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObj'(elementToVerify1)

            //TODO: make sure once id is added get error text and verify and display it in report log
            WebUI.verifyElementPresent(publisher_obj, 10)

            extentTest.log(Status.PASS, 'Verified Inline Validation For Empty PublisherName')

            break
        case 'newPackageData':
            CustomKeywords.'addPackageKeyword.AddPackageKeyword.addPackage'(extentTest, publisherName, packageName, installationLocation, 
                licenseFeaturePatterns, 'save')

            extentTest.log(Status.PASS, 'Package saved successfully with publisher: ' + publisherName)

            TestObject publisher_obj = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForPublisher'(publisherName)

            WebUI.scrollToElement(publisher_obj, 10)

            extentTest.log(Status.PASS, 'Scroll to publisher: ' + publisherName)

            WebUI.verifyElementPresent(publisher_obj, 10)

            extentTest.log(Status.PASS, 'Verified created publisher in the table: ' + publisherName)

            TestObject package_obj = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForPackage'(packageName)

            WebUI.verifyElementPresent(package_obj, 10)

            extentTest.log(Status.PASS, 'Verified created package in the table: ' + packageName)

            TestObject tracking_toggle_obj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObjForToggleButtonsForPackage'(
                packageName, 'tracking')

            boolean isTrackingToggleChecked = WebUI.verifyElementChecked(tracking_toggle_obj, 10)

            assert isTrackingToggleChecked == true

            extentTest.log(Status.PASS, 'Verified tracking toggle is checked by default: ' + isTrackingToggleChecked)

            TestObject inventory_toggle_obj = CustomKeywords.'generateTestObjs.createTestObj.generateTestObjForToggleButtonsForPackage'(
                packageName, 'inventory')

            boolean isInventoryToggleChecked = WebUI.verifyElementChecked(inventory_toggle_obj, 10)

            assert isInventoryToggleChecked == true

            extentTest.log(Status.PASS, 'Verified inventory toggle is checked by default: ' + isInventoryToggleChecked)

            //TODO: requires interaction to complete the active usage page
            break
        case 'editInstallationPath':
            CustomKeywords.'addPackageKeyword.AddPackageKeyword.addPackage'(extentTest, publisherName, packageName, installationLocation, 
                licenseFeaturePatterns, 'save')

            extentTest.log(Status.PASS, 'Package saved successfully with publisher: ' + publisherName)

            TestObject publisher_obj = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForPublisher'(publisherName)

            WebUI.scrollToElement(publisher_obj, 10)

            extentTest.log(Status.PASS, 'Scroll to publisher: ' + publisherName)

            WebUI.verifyElementPresent(publisher_obj, 10)

            extentTest.log(Status.PASS, 'Verified created publisher in the table: ' + publisherName)

            TestObject package_obj = CustomKeywords.'generateTestObjs.createTestObj.createTestObjForPackage'(packageName)

            WebUI.verifyElementPresent(package_obj, 10)

            extentTest.log(Status.PASS, 'Verified created package in the table: ' + packageName)

            TestObject editIcon = CustomKeywords.'generateTestObjs.createTestObj.generateEditTestObjForPackage'(packageName)

            WebUI.click(editIcon)

            extentTest.log(Status.PASS, 'Click the edit icon of the package: ' + packageName)

            String installationPathBeforeEditing = WebUI.getAttribute(findTestObject('Object Repository/Page_Packages/installation_location_field'), 
                'value')

            WebUI.verifyEqual(installationPathBeforeEditing, installationLocation)

            WebUI.setText(findTestObject('Object Repository/Page_Packages/installation_location_field'), elementToVerify2)

            extentTest.log(Status.PASS, 'Edit the installation path to new location: ' + elementToVerify2)

            //TODO: check this save button id, ambiguity issue
            WebUI.click(findTestObject('Object Repository/Page_Packages/edit_save_button'))

            extentTest.log(Status.PASS, 'Click the save icon of the package: ' + packageName)

            WebUI.click(editIcon)

            extentTest.log(Status.PASS, 'Click the edit icon of the package: ' + packageName)

            String installationPathAfterEditing = WebUI.getAttribute(findTestObject('Object Repository/Page_Packages/installation_location_field'), 
                'value')

            WebUI.verifyEqual(installationPathAfterEditing, elementToVerify2)

            break
    }
    
    driver.close()
}
catch (Exception ex) {
    String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'

    WebUI.takeScreenshot(screenShotPath)

    String p = (TestDescription + GlobalVariable.G_Browser) + '.png'

    extentTest.log(Status.FAIL, TestDescription + ' is failing')

    extentTest.log(Status.FAIL, ex)

    extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())

    KeywordUtil.markFailed('Exception Error: ' + ex)
} 
catch (StepErrorException e) {
    String screenShotPath = (('ExtentReports/' + TestDescription) + GlobalVariable.G_Browser) + '.png'

    WebUI.takeScreenshot(screenShotPath)

    String p = (TestDescription + GlobalVariable.G_Browser) + '.png'

    extentTest.log(Status.FAIL, TestDescription + ' is failing')

    extentTest.log(Status.FAIL, ex)

    extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())

    KeywordUtil.markFailed('StepErrorException Error: ' + e)
} 

