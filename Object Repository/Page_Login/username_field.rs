<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>username_field</name>
   <tag></tag>
   <elementGuidId>8ea4bfd5-5eb8-429f-807e-c742863cbe31</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='username']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#username</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>4c673ca4-2198-4cd6-ae2a-b79a268510fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-i18n-key</name>
      <type>Main</type>
      <value>placeholder</value>
      <webElementGuid>8e4ad4e5-95ca-4d2d-9d4a-23aec0e8f685</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>4d778919-5f1e-46f7-839a-d19f0cccb1ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>4d417b9a-d971-428b-b487-19e79bd8c048</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>db859774-c685-4f2f-94b4-c9c039fefe92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>login</value>
      <webElementGuid>d158f634-982a-45fe-9fd2-8e9e30b659e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>username theme-receiver</value>
      <webElementGuid>9812589d-9d6e-488f-9141-8b841e3cf96a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Username</value>
      <webElementGuid>93574f48-4b27-4bff-824a-6ee2a3600184</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;username&quot;)</value>
      <webElementGuid>3d9a5c97-48eb-4eb6-a567-6c4239ab9de2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='username']</value>
      <webElementGuid>fe612b06-c9d5-47d3-b074-b2c8452498e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='logindiv']/form/input</value>
      <webElementGuid>733e0d00-5e70-4fba-96e9-76a78642170f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>13874a30-8d58-4d39-ad81-555dc947f56f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @id = 'username' and @name = 'login' and @placeholder = 'Username']</value>
      <webElementGuid>ed91df10-3f00-4c4a-928e-873d8c8f53e1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
