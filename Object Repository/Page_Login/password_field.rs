<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>password_field</name>
   <tag></tag>
   <elementGuidId>47cbb2ab-ab88-43d8-8def-96cc36aafa49</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='password']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#password</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>67e8a097-3949-408d-b604-f4f7473ee551</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-i18n-key</name>
      <type>Main</type>
      <value>placeholder</value>
      <webElementGuid>55c4df02-e547-4f67-a1b3-3495d58b29dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>af5a78cb-9db2-4ea7-ac62-694361e16214</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>e19a56f5-2e09-4d37-846a-41a9530b129e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>3d00b1f7-a28b-423e-a9df-190e128d90f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>8fb965e2-a923-4349-ae70-d061413ab568</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>password theme-receiver</value>
      <webElementGuid>56e9c39d-d486-4b66-8e79-9921a0034121</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Password</value>
      <webElementGuid>ffd86e9f-b094-4490-837a-cdd6f622b344</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;password&quot;)</value>
      <webElementGuid>cc466a06-927c-40ad-98ab-e84dbd854311</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='password']</value>
      <webElementGuid>5c814963-7329-4582-9ee9-66c2862ede5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='logindiv']/form/input[2]</value>
      <webElementGuid>241f20f2-458a-49d7-a74b-61ccf502c41a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input[2]</value>
      <webElementGuid>07708cae-aaf4-4278-9cbf-d8f39e67ea5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password' and @id = 'password' and @name = 'password' and @placeholder = 'Password']</value>
      <webElementGuid>cc8c6ba9-0593-4e4e-a3f7-edd124874092</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
