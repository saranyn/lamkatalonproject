<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>apps_rounded_menu_icon</name>
   <tag></tag>
   <elementGuidId>1f003573-4ea2-4e8c-a6f5-a85e44d11c67</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>path</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[local-name()='svg' and @data-testid='AppsRoundedIcon']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
      <webElementGuid>ac619032-6b4d-491a-8b3c-fda2f5a6ea92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M4 8h4V4H4v4zm6 12h4v-4h-4v4zm-6 0h4v-4H4v4zm0-6h4v-4H4v4zm6 0h4v-4h-4v4zm6-10v4h4V4h-4zm-6 4h4V4h-4v4zm6 6h4v-4h-4v4zm0 6h4v-4h-4v4z</value>
      <webElementGuid>97987c87-a9cd-4ac4-a1c2-b922d17a966b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;white-theme MuiBox-root css-0&quot;]/header[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-elevation4 MuiAppBar-root MuiAppBar-colorPrimary MuiAppBar-positionStatic navbar-appbar tabs__labels css-1x7skt0&quot;]/div[@class=&quot;MuiContainer-root MuiContainer-disableGutters css-8d97to&quot;]/div[@class=&quot;MuiToolbar-root MuiToolbar-regular navbar-toolbar css-12o98wt&quot;]/button[@class=&quot;MuiButtonBase-root MuiIconButton-root MuiIconButton-sizeLarge css-1w8s6so&quot;]/svg[@class=&quot;MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-dwpknh&quot;]/path[1]</value>
      <webElementGuid>85e5040d-de62-4de1-9c8d-1fb182397ac4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
