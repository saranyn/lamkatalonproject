<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>module_object</name>
   <tag></tag>
   <elementGuidId>d26a94b3-94c9-4004-a6fb-7038fec3e9b7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '${module_name}' or . = '${module_name}')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-body1.MuiListItemText-primary.css-yb0lig</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[(text() = 'DSUA' or . = 'DSUA')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>3b8eeba0-fef1-4c89-89f7-1d14c8a716a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body1 MuiListItemText-primary css-yb0lig</value>
      <webElementGuid>bca614f5-3c96-48c8-9b16-9bd35ef43deb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>${module_name}</value>
      <webElementGuid>8f170b9b-9d6c-47b4-b3b3-b042acf07b9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;body&quot;)/div[@class=&quot;MuiDrawer-root MuiDrawer-modal MuiModal-root css-y28f86&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-elevation16 MuiDrawer-paper MuiDrawer-paperAnchorLeft css-wf16b5&quot;]/div[@class=&quot;MuiBox-root css-1hskriy&quot;]/ul[@class=&quot;MuiList-root MuiList-padding css-1ontqvh&quot;]/a[1]/li[@class=&quot;MuiListItem-root MuiListItem-gutters MuiListItem-padding more-app-list-item css-1yo8bqd&quot;]/div[@class=&quot;MuiListItemText-root more-app-link-text css-1tsvksn&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-body1 MuiListItemText-primary css-yb0lig&quot;]</value>
      <webElementGuid>ff72dd33-9b8d-479f-b618-3c29cde81224</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='body']/div[5]/div[3]/div/ul/a/li/div/span</value>
      <webElementGuid>b1d9d312-d9bf-4cb3-be50-06ce0791524e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='More Apps'])[1]/following::span[1]</value>
      <webElementGuid>ed54abe2-8a7b-4a0b-b7a3-82da31506183</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SLUAS'])[2]/preceding::span[1]</value>
      <webElementGuid>f75e149b-8f3f-4074-b432-ff73d9947405</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='License Server Control'])[1]/preceding::span[2]</value>
      <webElementGuid>15596c2b-828e-4cfe-b189-f485d89dcf0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='DSUA']/parent::*</value>
      <webElementGuid>4a6604ea-fa91-4b56-a296-25b04f64e844</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div/span</value>
      <webElementGuid>6707a24c-5162-49e0-a29d-81d3a582f139</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'DSUA' or . = 'DSUA')]</value>
      <webElementGuid>aeef5ead-0585-4ea4-a2c1-ef4a84010487</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
