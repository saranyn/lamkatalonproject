<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>add_package_button</name>
   <tag></tag>
   <elementGuidId>1506efed-86a1-496d-b3e1-0c6f1f904f45</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[normalize-space()='Add package']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
