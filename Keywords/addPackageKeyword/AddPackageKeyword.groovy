package addPackageKeyword
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.aventstack.extentreports.Status
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


class AddPackageKeyword {
	/**
	 * Refresh browser
	 */
	@Keyword
	def refreshBrowser() {
		KeywordUtil.logInfo("Refreshing")
		WebDriver webDriver = DriverFactory.getWebDriver()
		webDriver.navigate().refresh()
		KeywordUtil.markPassed("Refresh successfully")
	}

	/**
	 * Click element
	 * @param to Katalon test object
	 */
	@Keyword
	def clickElement(TestObject to) {
		try {
			WebElement element = WebUI.findWebElement(to);
			KeywordUtil.logInfo("Clicking element")
			element.click()
			KeywordUtil.markPassed("Element has been clicked")
		} catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Element not found")
		} catch (Exception e) {
			KeywordUtil.markFailed("Fail to click on element")
		}
	}

	/**
	 * Get all rows of HTML table
	 * @param table Katalon test object represent for HTML table
	 * @param outerTagName outer tag name of TR tag, usually is TBODY
	 * @return All rows inside HTML table
	 */
	@Keyword
	def List<WebElement> getHtmlTableRows(TestObject table, String outerTagName) {
		WebElement mailList = WebUI.findWebElement(table)
		List<WebElement> selectedRows = mailList.findElements(By.xpath("./" + outerTagName + "/tr"))
		return selectedRows
	}

	@Keyword
	def addPackage(def extentTest, String packagePublisher, String packageName, String installationLocation, String licenseFeaturePatterns, String flag) {

		WebUI.click(findTestObject('Object Repository/Page_Packages/add_package_button'))
		extentTest.log(Status.PASS, 'Click Add Package button')
		WebUI.setText(findTestObject('Object Repository/Page_Packages/package_publisher_field'), packagePublisher)
		extentTest.log(Status.PASS, 'Enter Publisher Name: ' +packagePublisher)
		WebUI.setText(findTestObject('Object Repository/Page_Packages/package_name_field'), packageName)
		extentTest.log(Status.PASS, 'Enter Package Name: ' +packageName)
		WebUI.setText(findTestObject('Object Repository/Page_Packages/installation_location_field'), installationLocation)
		extentTest.log(Status.PASS, 'Enter Installation Location: ' +installationLocation)
		WebUI.setText(findTestObject('Object Repository/Page_Packages/license_feature_patterns_field'), licenseFeaturePatterns)
		extentTest.log(Status.PASS, 'Enter License Feature Patterns: ' +licenseFeaturePatterns)
		if(flag == "save") {
			WebUI.click(findTestObject('Object Repository/Page_Packages/save_button'))
			extentTest.log(Status.PASS, 'Click Save Package button')
		} else if(flag == "cancel") {
			WebUI.click(findTestObject('Object Repository/Page_Packages/cancel_button'))
			extentTest.log(Status.PASS, 'Click Cancel button')
		}
	}
}