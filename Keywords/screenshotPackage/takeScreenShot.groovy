package screenshotPackage

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.awt.image.BufferedImage
import java.nio.file.Paths

import javax.imageio.ImageIO

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import kms.turing.ru.yandex.qatools.ashot.comparison.ImageDiff
//import kms.turing.ru.yandex.qatools.ashot.comparison.ImageDiffer


public class takeScreenShot {



	def takeScreenshot(screenshotFolderPath, imageName, extetnTest) {

		println("===================================================================================================")
		println('screenshotFolderPath = '+screenshotFolderPath)
		println('imageName = '+imageName)
		String screenshotFileName = imageName+'.png'
		String screenshotFilePath = Paths.get(screenshotFolderPath, screenshotFileName).toString()
		//def nameOfFile=screenshotFilePath+screenshotFileName
		println('screenshotFileName ='+screenshotFileName)
		println('screenshotFilePath ='+screenshotFilePath)
		//println('nameOfFile = '+nameOfFile)
		println("===================================================================================================")
		TestObject elementToCapture = findTestObject('Object Repository/Plot/PlotArea')
		String path=	WebUI.takeElementScreenshot(screenshotFilePath,elementToCapture,FailureHandling.CONTINUE_ON_FAILURE)
		println("===================================================================================================")
		println("Screenshot of the element saved location and returning this =  " + screenshotFilePath)
		println("===================================================================================================")
		return screenshotFilePath
	}



	def compareScreenShot(screenshotFolderPath,expectedImageFilePath,extentTest) {

		println("===================================================================================================")
		println('expected image - '+expectedImageFilePath)
		println("===================================================================================================")
		String actualImage=(new screenshotPackage.takeScreenShot()).takeScreenshot(screenshotFolderPath, 'actual-user-01', extentTest)
		println("===================================================================================================")
		println('actual image - '+actualImage)
		println("===================================================================================================")


		BufferedImage expected = ImageIO.read(new File(expectedImageFilePath))
		BufferedImage actual = ImageIO.read(new File(actualImage))

		//		ImageDiffer imgDiff = new ImageDiffer()
		//		ImageDiff diff = imgDiff.makeDiff(actual, expected)
		def diffPath=screenshotFolderPath+'/diff.png'
		File diffImgeFile = new File (diffPath)


		if(diff.hasDiff()) {
			println("===================================================================================================")
			println("Image is DIFFERENT.");
			BufferedImage diffImage=diff.getMarkedImage()
			println('diff image file wala = '+diffPath)
			ImageIO.write(diffImage, "png",diffImgeFile)
			println("===================================================================================================")
		}
		else {
			println("===================================================================================================")
			println("Image is SAME.");
			println("===================================================================================================")
		}
	}
}
