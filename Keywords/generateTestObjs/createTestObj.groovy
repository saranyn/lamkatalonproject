package generateTestObjs

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject


public class createTestObj {


	@Keyword
	def generateTestObj(String testObj_xpath) {

		println testObj_xpath

		// Building job indentifier obj
		TestObject testObj = new TestObject('objectName')
		testObj.addProperty('xpath', ConditionType.EQUALS, testObj_xpath)

		return testObj
	}

	@Keyword
	def createTestObjForModule(String module_name) {

		def testObj_xpath = "//span[(text() = '"+module_name+"' or . = '"+module_name+"')]"

		// Building job indentifier obj
		TestObject testObj = new TestObject('objectName')
		testObj.addProperty('xpath', ConditionType.EQUALS, testObj_xpath)

		return testObj
	}



	@Keyword
	def createTestObjForComputersByStatus(String elementId, String hoverElement) {

		def testObj_xpath = "//div[@id='"+elementId+"']//div//span[text()='"+hoverElement+"']/following-sibling::span"

		// Building job indentifier obj
		TestObject testObj = new TestObject('objectName')
		testObj.addProperty('xpath', ConditionType.EQUALS, testObj_xpath)

		return testObj
	}

	@Keyword
	def createTestObjForPublisher(String publisherName) {

		def testObj_xpath = "//tbody/tr/td[1]/span[@title='"+publisherName+"']"

		// Building job indentifier obj
		TestObject testObj = new TestObject('objectName')
		testObj.addProperty('xpath', ConditionType.EQUALS, testObj_xpath)

		return testObj
	}

	@Keyword
	def createTestObjForPackage(String packageName) {

		def testObj_xpath = "//tbody/tr/td[3]/span[@title='"+packageName+"']"

		// Building job indentifier obj
		TestObject testObj = new TestObject('objectName')
		testObj.addProperty('xpath', ConditionType.EQUALS, testObj_xpath)

		return testObj
	}

	@Keyword
	def generateTestObjForToggleButtonsForPackage(String packageName, String toggleName) {

		def testObj_xpath

		if (toggleName == 'tracking') {
			testObj_xpath = "//tbody/tr/td[3]/span[@title='"+packageName+"']/following::input[@type='checkbox'][1]"
		} else if (toggleName == 'inventory') {
			testObj_xpath = "//tbody/tr/td[3]/span[@title='"+packageName+"']/following::input[@type='checkbox'][2]"
		}

		// Building job indentifier obj
		TestObject testObj = new TestObject('objectName')
		testObj.addProperty('xpath', ConditionType.EQUALS, testObj_xpath)

		return testObj
	}



	@Keyword
	def generateEditTestObjForPackage(String packageName) {

		def testObj_xpath = "//tbody/tr/td[3]/span[@title='"+packageName+"']/following::*[name()='svg' and @data-testid='EditRoundedIcon'][1]"

		// Building job indentifier obj
		TestObject testObj = new TestObject('objectName')
		testObj.addProperty('xpath', ConditionType.EQUALS, testObj_xpath)

		return testObj
	}

	@Keyword
	def generateTestObjForNavigationLink(String linkName) {

		def testObj_xpath;

		if (linkName == "Package History") {
			testObj_xpath = "//h5[normalize-space()='"+linkName+"']"
		} else {
			testObj_xpath = "//h4[normalize-space()='"+linkName+"']"
		}

		// Building job indentifier obj
		TestObject testObj = new TestObject('objectName')
		testObj.addProperty('xpath', ConditionType.EQUALS, testObj_xpath)

		return testObj
	}
}



