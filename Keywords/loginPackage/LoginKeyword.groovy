package loginPackage
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.aventstack.extentreports.Status
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

class LoginKeyword {
	/**
	 * Refresh browser
	 */
	@Keyword
	def refreshBrowser() {
		KeywordUtil.logInfo("Refreshing")
		WebDriver webDriver = DriverFactory.getWebDriver()
		webDriver.navigate().refresh()
		KeywordUtil.markPassed("Refresh successfully")
	}

	/**
	 * Click element
	 * @param to Katalon test object
	 */
	@Keyword
	def clickElement(TestObject to) {
		try {
			WebElement element = WebUI.findWebElement(to);
			KeywordUtil.logInfo("Clicking element")
			element.click()
			KeywordUtil.markPassed("Element has been clicked")
		} catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Element not found")
		} catch (Exception e) {
			KeywordUtil.markFailed("Fail to click on element")
		}
	}

	/**
	 * Get all rows of HTML table
	 * @param table Katalon test object represent for HTML table
	 * @param outerTagName outer tag name of TR tag, usually is TBODY
	 * @return All rows inside HTML table
	 */
	@Keyword
	def List<WebElement> getHtmlTableRows(TestObject table, String outerTagName) {
		WebElement mailList = WebUI.findWebElement(table)
		List<WebElement> selectedRows = mailList.findElements(By.xpath("./" + outerTagName + "/tr"))
		return selectedRows
	}

	@Keyword
	def login(def extentTest, String username, String password) {
		try {
			WebUI.setText(findTestObject('Object Repository/Page_Login/username_field'), username)
			extentTest.log(Status.PASS, 'Enter username: '+username)
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Login/password_field'), password)
			extentTest.log(Status.PASS, 'Enter password: '+password)
			WebUI.click(findTestObject('Object Repository/Page_Login/login_btn'))
			extentTest.log(Status.PASS, 'Click Login button')
		} catch (Exception e) {
			KeywordUtil.markFailed("Login keyword failed due to: " +e)
		}
	}
	
	@Keyword
	def loginInvalid(def extentTest, String username, String password) {
		try {
			WebUI.setText(findTestObject('Object Repository/Page_Login/username_field'), username)
			extentTest.log(Status.PASS, 'Enter username: '+username)
			WebUI.setEncryptedText(findTestObject('Object Repository/Page_Login/password_field'), password)
			extentTest.log(Status.PASS, 'Enter password: '+password)
			WebUI.click(findTestObject('Object Repository/Page_Login/login_btn'))
			extentTest.log(Status.PASS, 'Click Login button')
			String error_actual = WebUI.getText(findTestObject('Object Repository/Page_Login/invalid_login_error'))
			return error_actual
		} catch (Exception e) {
			KeywordUtil.markFailed("Login keyword failed due to: " +e)
		}
	}

	@Keyword
	def navigateToModule(WebDriver driver, def extentTest, TestObject module_test_obj, String dsua_url) {

		WebUI.click(module_test_obj)

		extentTest.log(Status.PASS, 'Click DSUA module')

		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles())

		driver.switchTo().window(tabs.get(1))

		WebUI.waitForElementPresent(findTestObject('Object Repository/Page_Altair_DSUA/user_icon_menu'), 25)

		extentTest.log(Status.PASS, 'Switch to DSUA url')

		WebUI.delay(10)

		String url = driver.getCurrentUrl()

		println(url)

		assert url == dsua_url

		extentTest.log(Status.PASS, 'Verify DSUA url: '+url)

		driver.get(url)
	}
}