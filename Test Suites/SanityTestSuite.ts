<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SanityTestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>81978a52-3ccc-481a-85b3-85647bcc1c4b</testSuiteGuid>
   <testCaseLink>
      <guid>442c882e-0839-4b62-8bfc-9a2c394f6965</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Generic/LoginTestCase</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>56dfd41f-c0cb-466d-afde-8162ac704cc3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f5c2d165-77fa-4b68-b555-c77a8f581634</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>db41344b-5868-4eb7-832c-8807e20a5327</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9aa865f6-c9f5-4ccd-bc50-adf075464a27</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2ab318b7-45e6-4460-b192-3b5a8ad20cb4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>06bc5aa8-e2b1-4764-b324-f7059f5d9b17</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>29c27592-d78a-4f78-86cd-f631bff1f14a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Generic/LogoutTestCase</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2f8f326d-30ac-4829-aaaa-bb1f9c471d3f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2fef44c4-611e-434c-b769-7d68feadbe12</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>bcdb38bd-c707-4bd3-bc93-63ed9d42aa6d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Generic/LoginAndNavigateToDSUA</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b8439322-8c0e-4e54-8342-2e65f71dff2f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>aac0f942-4485-41e0-8549-1bad43af3bc5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5cb971ff-a480-47a4-b2c4-9519dc5bd1f9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a0ed72d7-45c4-4712-a581-6f4bc6655aa5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b0f4ff85-2883-4221-8dff-952246713bb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <iterationNameVariable>
         <defaultValue>'Add Package'</defaultValue>
         <description></description>
         <id>ae3c7588-acba-4301-8779-2948938a3ea0</id>
         <masked>false</masked>
         <name>TestDescription</name>
      </iterationNameVariable>
      <testCaseId>Test Cases/Packages/AddPackageTestCase</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f837287c-7c87-4ae0-a51d-1d026787ea40</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value>1</value>
         </iterationEntity>
         <testDataId>Data Files/TestDataForAddPackage</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>f837287c-7c87-4ae0-a51d-1d026787ea40</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>TestDescription</value>
         <variableId>ae3c7588-acba-4301-8779-2948938a3ea0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ec7ea7d9-2f81-4571-9328-7d705090549e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4f7ef6d6-77c8-4bc0-8dcd-5d21646865d4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f837287c-7c87-4ae0-a51d-1d026787ea40</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>publisherName</value>
         <variableId>43563b43-a426-4d8a-ae57-e36b5bc86722</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f837287c-7c87-4ae0-a51d-1d026787ea40</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>packageName</value>
         <variableId>55d55c9a-0e11-4dd9-8e51-e083d5d12f65</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f837287c-7c87-4ae0-a51d-1d026787ea40</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>installationLocation</value>
         <variableId>4883c7b1-8594-4b81-bffa-75ce74d22112</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f837287c-7c87-4ae0-a51d-1d026787ea40</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>licenseFeaturePatterns</value>
         <variableId>c57ea72d-50e3-430d-8cc2-bda9948e6b9d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f837287c-7c87-4ae0-a51d-1d026787ea40</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>TestCaseID</value>
         <variableId>e0df4242-faab-4044-b905-32237a15f098</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f837287c-7c87-4ae0-a51d-1d026787ea40</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>userChoice</value>
         <variableId>6de3f951-a7e9-44b0-b423-5fff49fc527e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f837287c-7c87-4ae0-a51d-1d026787ea40</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>elementToVerify1</value>
         <variableId>94a1bd7f-45ba-429d-80ce-f086810f4ee0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f837287c-7c87-4ae0-a51d-1d026787ea40</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>elementToVerify2</value>
         <variableId>67d46b5b-fb90-4c60-aae7-edd6b8da3d9b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>900599df-a219-4173-871e-a65e8217f2f9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8af27f85-b60f-4b14-878a-9fe65f0183f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Generic/PageNavigationLinkTestCase</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c4bf0323-aff0-40a5-9970-9ba3ed9a4b24</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>612733c8-727c-4307-bb34-dc13defe4df1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bf37c78a-5ff5-43ed-8086-8c94b622650a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>461a8ec8-7e10-46f9-96e4-8dfe44aeb9e5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b31253e8-5ce3-4d68-b83c-e30b917c52b5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>52b08b6a-da03-49be-a2bd-16068d4b7e9f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>304dc81b-e45e-4e20-9f06-d298c545a231</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9d4de377-cf93-4b0d-9656-70c7a68e76b3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>369378c9-f434-443c-8c2b-16679657cf97</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>61f51f5c-67ce-4f1a-bcce-433dbe7cd37c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d822d214-4ccd-4d64-b336-bcd19f9898cc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>731db1b6-b9e3-4a11-b107-020ef4ee8ddb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ab9d61fd-68d7-435a-abbf-bf3dccc4ec23</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>711f4e76-b22a-44a5-abcc-2e86f2c77455</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>03f9bb95-9c60-4b64-bcce-97e1af14452e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <iterationNameVariable>
         <defaultValue>'Add Package'</defaultValue>
         <description></description>
         <id>f8bd9a58-b643-40c0-90ec-d6e62fb82658</id>
         <masked>false</masked>
         <name>TestDescription</name>
      </iterationNameVariable>
      <testCaseId>Test Cases/Home/PieChartTestCase</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c6b75773-e5c8-4a16-9c5b-082edb580980</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value>4</value>
         </iterationEntity>
         <testDataId>Data Files/TestDataForPieChart</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>c6b75773-e5c8-4a16-9c5b-082edb580980</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>TestCaseID</value>
         <variableId>e2c8d73e-998a-4203-93fd-d34f6c613f83</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>c6b75773-e5c8-4a16-9c5b-082edb580980</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>TestDescription</value>
         <variableId>f8bd9a58-b643-40c0-90ec-d6e62fb82658</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>c6b75773-e5c8-4a16-9c5b-082edb580980</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>elementId</value>
         <variableId>e0130fcd-9e3c-4bb2-bb32-cead8a9f1699</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>c6b75773-e5c8-4a16-9c5b-082edb580980</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>fileName</value>
         <variableId>ae91b314-e9e8-45b0-a381-f6e989d0c9f5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c82849e9-8358-4236-b285-4d06b3605867</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2792730c-e10d-445f-aadc-fd4043981f81</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c9238904-885c-40cf-b875-21ff5dcea6ff</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>c6b75773-e5c8-4a16-9c5b-082edb580980</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>hoverElement1</value>
         <variableId>8a8b56ea-e23e-488e-b528-baabdea11a48</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>c6b75773-e5c8-4a16-9c5b-082edb580980</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>hoverElement2</value>
         <variableId>3adf42d4-9292-4608-a03b-d68ec4be8ebc</variableId>
      </variableLink>
   </testCaseLink>
<<<<<<< HEAD
   <testCaseLink>
      <guid>6a536a66-cf39-449b-98d7-d95b82487534</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <iterationNameVariable>
         <defaultValue>'Login to LAM'</defaultValue>
         <description></description>
         <id>8a3ce997-15be-4e90-99a9-d81c50798da5</id>
         <masked>false</masked>
         <name>TestDescription</name>
      </iterationNameVariable>
      <testCaseId>Test Cases/Generic/LoginTestCase - InvalidCase</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8037ccf6-6fb9-4cee-8547-97868d2a39f4</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TestDataForInvalidLogin</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>8037ccf6-6fb9-4cee-8547-97868d2a39f4</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>32ebf6ba-d4c5-42d8-933e-9049c2c6d70d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8037ccf6-6fb9-4cee-8547-97868d2a39f4</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>c088ea95-9519-438e-8c12-d8dd010842dd</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8037ccf6-6fb9-4cee-8547-97868d2a39f4</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>TestDescription</value>
         <variableId>8a3ce997-15be-4e90-99a9-d81c50798da5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>61761356-8a81-4c56-8f19-c22d8e9395b5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>156bf2f0-656a-47fd-b8c6-136e440160e7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8037ccf6-6fb9-4cee-8547-97868d2a39f4</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>elementToVerify</value>
         <variableId>434336e9-3013-4d8f-b239-9b7ba08f9b5a</variableId>
      </variableLink>
   </testCaseLink>
=======
>>>>>>> f600749a368de1bbe7d52df89ab84000e38f07d5
   <testCaseLink>
      <guid>d4e91f98-f96a-43e7-999d-56cad0708849</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Generic/TC002-FilterPackageName</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dd8e1c9e-f6ac-4989-9d9a-765d9ce6451e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e94ea806-715a-470c-88fb-8b5fb7bc3dac</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>08c2628a-7949-4405-8418-6f1d29b712ac</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>66f17bab-e5d5-4122-ad65-bccf68bbf982</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f808cd81-8b2d-43b2-b798-58e8e275b14a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>836123cf-bcb0-4021-99aa-cb4395cfc92c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Generic/TC007-GoBackButton</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6ed64080-e3ac-4191-9c00-f917f35a2ced</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ca54b4c2-be1a-4b3c-bfd8-6f43632a0c69</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>131cf5b1-9359-4384-aa3a-38da940d2e1e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e808c907-3001-46d9-bcc8-756fb8d7cbf9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f950dd29-bfb1-476c-aa80-ce991a6bcd77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Generic/TC004-DownloadDSUAagent</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>812fdafb-dd5d-4ae3-be85-2f405e400e40</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5aefd1bc-671e-4b15-b0f7-9e69720a3fa4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7f4d1320-38f9-4e28-a026-e8ec7a65288d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fea40238-7489-42f8-b4f6-e617271df78d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2d0517ba-bf42-4e49-b66b-b77d96a22be7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a5d5a5cc-117f-4e79-964d-e47e48409827</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
